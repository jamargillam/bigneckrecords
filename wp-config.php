<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bigneckrecords');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VZ`if:.:+nf+d(^&*:::SaXhuo?*m|-6O/#qZ`Xke48(rsI^NH_QAU@moce+rBQ@');
define('SECURE_AUTH_KEY',  'D&C~j.nKv%l$APapqetBdC2-g<T0{zVZ}X&-(XupO(=6Or$J||XG#!x-yN:&a;lI');
define('LOGGED_IN_KEY',    '#xma +r+MQBsb1]ax-/sc)zmlV}+}vJ&~|V;# [`qt7LmJ8p<1^RwS,pn)Cl{L?]');
define('NONCE_KEY',        's.(R<S1^?97O H/OZtKmZ0ponW3L~ .G+OaOTdy#}>2vs`~O[}2bQiy-5F0vUcjS');
define('AUTH_SALT',        'x$JBoQdh~pR3ff:L;19Q]RcOUpgIato/|i?T|@V[e_?9#AX r`Tv=P_[Vy_/QO[D');
define('SECURE_AUTH_SALT', 'RB:Ipmj3. [j!2E+,R%?7Fj$)--DDqy1%*l#Z<-PkydQ+r+ 1s  <Xz(6]%6.`&6');
define('LOGGED_IN_SALT',   '7R@j5HfKu3j0(NB;b-ITV3S!X3<q/:d7X+h{BhHM;N!=W.FX2G,tK%y Y.(zdjR0');
define('NONCE_SALT',       '#<CKW$:-`8|Ss9&HN6%1Mc<7iycXNx+gW)o[g-wDta}Uc1z@td5Hyaf-P:y{Q<Y^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
