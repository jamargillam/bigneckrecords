<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( ' | ', true, 'right' ); ?></title>
<link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap.css" />
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css" />
<?php wp_head(); ?>
</head>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-4"><a href="<?php echo site_url(); ?>/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bigneck-logo.png" class="img-responsive" title="Big Neck Records" /></a></div>
            <div class="col-md-8">
                <ul class="nav nav-justified">
                    <li><a href="https://soundcloud.com/bigneckrecords">ARTISTS</a></li>
                    <li><a href="<?php echo site_url(); ?>/releases">RELEASES</a></li>
                    <li><a href="<?php echo site_url(); ?>/tours">TOURS</a></li>
                    <li><a href="<?php echo site_url(); ?>/store">STORE</a></li>
                    <li><a href="<?php echo site_url(); ?>/blog">Blog</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- /#header -->
<body <?php body_class(); ?>>
