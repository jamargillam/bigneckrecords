<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<section id="hero">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="hero-inner-wrap">
                    <h1>PRE-ORDER BASEBALL FURY'S NEW ALBUM</h1>
                    <p class="intro">Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                    <a href="<?php echo site_url(); ?>/store" class="button btn red-btn">GO TO STORE</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /#hero -->

<section id="featured-artists">
    <div class="container">
        <div class="row">
          <?php get_template_part( 'featured-artists-widget' ); ?>
        </div>
    </div>
</section>
<!-- /#featured-artists -->

<section id="latest-news">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
              <h3>Latest News</h3>
                <?php
                  $args = array('numberposts' => 3, 'order' => 'DESC', 'orderby' => 'post_date');
                  $postslist = get_posts($args);
                  foreach ($postslist as $post) : setup_postdata($post);
                ?>
                <article>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="<?php the_permalink(); ?>" class="article-img"><img src="" alt="" class="img-responsive" /></a>
                        </div>
                        <div class="col-md-8">
                            <div class="post-content">
                                <span class="post-date"><?php echo get_the_date("F j, Y"); ?></span>
                                <h4 class="post-title" title="<?php echo the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p class="post-conent">
                                    <?php echo the_content(); ?>
                                </p>
                            </div>
                      </div>
                    </div>
                </article>
              <?php endforeach; ?>
            </div>
            <div class="col-md-4">
                <?php get_template_part( 'follow-us-sidebar' ); ?>
                <?php get_template_part( 'upcoming-shows-sidebar' ); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
