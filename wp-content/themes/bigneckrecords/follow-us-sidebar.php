<aside id="follow-us">
    <p>FOLLOW US: </p>
    <ul class="list-inline">
        <li><a target="_blank" href="https://www.facebook.com/bigneckrecords/" class="social-icon text-center" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" href="https://twitter.com/bigneckrecords" class="social-icon text-center" title="Twitter"><i class="fa fa-twitter"></i></a></li>
        <li><a target="_blank" href="https://www.youtube.com/user/bigneckrecords" class="social-icon text-center" title="Youtube"><i class="fa fa-youtube"></i></a></li>
    </ul>
</aside>
<!-- /#follow-us -->
