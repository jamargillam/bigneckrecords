/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'),
    phplint = require('phplint').lint,
    uglify = require('gulp-uglify');

var jslint = require('gulp-jslint');

var port = 80;
livereload.listen(80);
// create a default task and just log a message
gulp.task('default', ['watch-php']);

gulp.task('minify-js', function () {
    gulp.src('assets/js/*.js') // path to your files
    .pipe(uglify())
    .pipe(livereload())
    .pipe(gulp.dest('assets/js'));
    console.log('mini js');
});

gulp.task('watch-php', function () {
    gulp.src('./*.php') // path to your files
    .pipe(livereload())
    console.log('watch php files');
});

// build the main source into the min file
gulp.task('jslint', function () {
    return gulp.src(['/assets/js'])

        // pass your directives
        // as an object
        .pipe(jslint({
            // these directives can
            // be found in the official
            // JSLint documentation.
            node: true,
            evil: true,
            nomen: true,

            // you can also set global
            // declarations for all source
            // files like so:
            global: [],
            predef: [],
            // both ways will achieve the
            // same result; predef will be
            // given priority because it is
            // promoted by JSLint

            // pass in your prefered
            // reporter like so:
            reporter: 'jslint',
            // ^ there's no need to tell gulp-jslint
            // to use the default reporter. If there is
            // no reporter specified, gulp-jslint will use
            // its own.

            // specifiy custom jslint edition
            // by default, the latest edition will
            // be used
            edition: '2014-07-08',

            // specify whether or not
            // to show 'PASS' messages
            // for built-in reporter
            errorsOnly: false
        }))

        // error handling:
        // to handle on error, simply
        // bind yourself to the error event
        // of the stream, and use the only
        // argument as the error object
        // (error instanceof Error)
        .on('error', function (error) {
            console.error(String(error));
        });
});

var watcher = gulp.watch('assets/js/**/*.js', ['jslint', 'watch-php']);
watcher.on('change', function(event) {
  livereload.listen(80);
  livereload.reload('home.php');
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
});

/* gulp.watch('/assets/js/*.js', function() {
    gulp.run('jslint', 'scripts');
    console.log('watching for js changes....');
  }); */
