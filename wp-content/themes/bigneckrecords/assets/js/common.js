$(document).ready(function(){
  var tours = $("#tours");
  if (tours.length > 0) {
    $(".vsel-meta-link").children("a").addClass("btn");
  }

  //Remvoe "Date:"
  $.each($(".vsel-meta-date"), function(index, element){
    var date = $(this).text().replace(/Date:/g, "");
    $(this).text(date);
  });

  //Remvoe "Location:"
  $.each($(".vsel-meta-location"), function(index, element){
    var locaton = $(this).text().replace(/Location:/g, "");
    $(this).text(locaton);
  });
  
});
