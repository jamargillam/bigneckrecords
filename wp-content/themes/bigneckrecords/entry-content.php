<div class="post-date">
  <i class="fa fa-calendar" aria-hidden="true"></i>
  <?php the_time( get_option( 'date_format' ) ); ?>
</div>
 <?php if (has_post_thumbnail()) { ?>
  <a href="<?php the_permalink(); ?>" class="feat-img"title="<?php the_title(); ?>">
    <?php the_post_thumbnail(); ?>
  </a>
 <?php } ?>
<div class="post-excerpt"><?php the_excerpt();?></div>
<?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'blankslate' ) . '&after=</div>') ?>
