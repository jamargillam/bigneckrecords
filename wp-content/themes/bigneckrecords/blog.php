<?php /* Template Name: Blog */ ?>
<?php get_header(); ?>


<section id="blog">
  <div class="container">
    <div class="row">
      <h2></h2>
      <div class="col-md-8">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array('post_type' => 'post', 'posts_per_page' => 5, 'paged' => $paged);
        $wp_query = new WP_Query($args);
        while (have_posts()) : the_post();
        ?>
        <?php get_template_part('entry'); ?>
        <?php endwhile; ?>
      </div>
      <div class="col-md-4">
        <?php get_template_part( 'follow-us-sidebar' ); ?>
        <?php get_template_part( 'upcoming-shows-sidebar' ); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
