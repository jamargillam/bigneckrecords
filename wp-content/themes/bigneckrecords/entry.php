<article class="post text-center">
    <h2>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
    </h2>
    <?php
    if (is_archive() || is_search()) {
        get_template_part('entry', 'summary');
    } else {
        get_template_part('entry', 'content');
    }
    ?>
    <?php
    if (is_single()) {
        get_template_part('entry-footer', 'single');
    } else {
        get_template_part('entry-footer');
    }
    ?>
    <button class="btn btn-large btn-default" href="<?php the_permalink(); ?>" title="<?php the_title();?>">Read full article...</button>
</article>
