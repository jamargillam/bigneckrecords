<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3"><a href="<?php echo site_url(); ?>/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bigneck-logo.png" class="img-responsive" title="Big Neck Records" /></a></div>
            <div class="col-md-4">
                <ul class="nav nav-justified">
                    <li><a href="<?php echo site_url(); ?>/artists">ARTISTS</a></li>
                    <li><a href="<?php echo site_url(); ?>/releases">RELEASES</a></li>
                    <li><a href="<?php echo site_url(); ?>/tours">TOURS</a></li>
                    <li><a href="<?php echo site_url(); ?>/store">STORE</a></li>
                    <li><a href="<?php echo site_url(); ?>/blog">Blogs</a></li>
                </ul>
            </div>
            <div class="col-md-5">
                <p class="text-right"><?php echo "&copy;" . date('Y') . ". All Rights Reserved." ?></p>
            </div>
        </div>
    </div>
</footer><!-- /#footer -->
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/common.js"></script>
</body>
</html>
