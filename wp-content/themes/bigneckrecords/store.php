<?php /* Template Name: Store */ ?>
<?php get_header(); ?>

<section id="store">
  <div class="container">
    <div class="row">
      <h1>Store</h1>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
      the_content();
      endwhile; else: ?>
      <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
