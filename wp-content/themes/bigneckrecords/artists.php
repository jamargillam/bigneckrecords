<?php /* Template Name: Artists */ ?>
<?php get_header(); ?>

<section id="artists-hero">
  <div class="container">
    <div class="row">
      <?php get_template_part( 'featured-artists-widget' ); ?>
    </div>
  </div>
</section>

<section id="artists">
  <div class="container">
    <div class="row">
      <h2>Artists</h2>
      <div class="col-md-8">
        <div class="artist">

        </div>
        <!-- /.artist -->
      </div>
      <div class="col-md-4">
        <?php get_template_part( 'follow-us-sidebar' ); ?>
        <?php get_template_part( 'upcoming-shows-sidebar' ); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
