<?php /* Template Name: Tours */ ?>
<?php get_header(); ?>
<section id="tours-hero">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Some tour</h1>
      </div>
    </div>
  </div>
</section>
<section id="tours">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="text-center">Upcoming Shows</h2>
        <?php echo do_shortcode( '[vsel posts_per_page=10]' ); ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
