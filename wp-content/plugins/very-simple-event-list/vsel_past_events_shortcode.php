<?php

// The shortcode
function vsel_past_events_shortcode( $vsel_atts ) {
	$vsel_atts = shortcode_atts( array( 
		'event_cat' => '', // include certain event categories
		'posts_per_page' => '' // set posts per page
	), $vsel_atts );

$output = ""; 
$output .= '<div id="vsel">'; 

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$today = strtotime('today'); 

	$vsel_meta_query = array( 
		'relation' => 'AND',
		array( 
			'key' => 'event-date', 
			'value' => $today, 
			'compare' => '<' 
		) 
	); 

	$vsel_query_args = array( 
		'post_type' => 'event', 
		'event_cat' => $vsel_atts['event_cat'],
		'post_status' => 'publish', 
		'ignore_sticky_posts' => true, 
		'meta_key' => 'event-date', 
		'orderby' => 'meta_value_num', 
		'order' => 'desc',
		'posts_per_page' => $vsel_atts['posts_per_page'],
 		'paged' => $paged, 
		'meta_query' => $vsel_meta_query
	); 

	$vsel_events = new WP_Query( $vsel_query_args );

	if ( $vsel_events->have_posts() ) : 
		while( $vsel_events->have_posts() ): $vsel_events->the_post(); 
	
		$event_date = get_post_meta( get_the_ID(), 'event-date', true ); 
		$event_date_hide = get_post_meta( get_the_ID(), 'event-date-hide', true );
		$event_time = get_post_meta( get_the_ID(), 'event-time', true ); 
		$event_location = get_post_meta( get_the_ID(), 'event-location', true ); 
		$event_link = get_post_meta( get_the_ID(), 'event-link', true ); 
		$event_link_label = get_post_meta( get_the_ID(), 'event-link-label', true ); 

		if (empty($event_link_label)) {
			$event_link_label = esc_attr__( 'More info', 'very-simple-event-list' );
		}

		// display the event list
		$output .= '<div class="vsel-content">'; 
			$output .= '<div class="vsel-meta">'; 
				$output .= '<h4 class="vsel-meta-title">' . get_the_title() . '</h4>';
				if ($event_date_hide != 'yes') {
					$output .= '<p class="vsel-meta-date">';
					$output .= sprintf(esc_attr__( 'Date: %s', 'very-simple-event-list' ), date_i18n( get_option( 'date_format' ), esc_attr($event_date) ) ); 
					$output .= '</p>';
				}
				if(!empty($event_time)){
					$output .= '<p class="vsel-meta-time">';
					$output .= sprintf(esc_attr__( 'Time: %s', 'very-simple-event-list' ), esc_attr($event_time) ); 
					$output .= '</p>';
				}
				if(!empty($event_location)){
					$output .= '<p class="vsel-meta-location">';
					$output .= sprintf(esc_attr__( 'Location: %s', 'very-simple-event-list' ), esc_attr($event_location) ); 
					$output .= '</p>';
				}
				if(!empty($event_link)){
					$output .= '<p class="vsel-meta-link">';
					$output .= sprintf( '<a href="%1$s" target="_blank">%2$s</a>', esc_url($event_link), esc_attr($event_link_label) ); 
					$output .= '</p>';
				}
			$output .= '</div>';
			$output .= '<div class="vsel-info">';
				if ( has_post_thumbnail() ) { 
					$output .=  get_the_post_thumbnail(); 
				} 
				$output .= $content = apply_filters( 'the_content', get_the_content() ); 
			$output .= '</div>';
		$output .= '</div>';
	
		endwhile; 
	
		// pagination
		$output .= get_next_posts_link(  __( 'Next &raquo;', 'very-simple-event-list' ), $vsel_events->max_num_pages ); 
		$output .= get_previous_posts_link( __( '&laquo; Previous', 'very-simple-event-list' ) ); 

		wp_reset_postdata(); 

		else:
 
		$output .= '<p>';
		$output .= esc_attr__('There are no past events.', 'very-simple-event-list');
		$output .= '</p>';
	endif; 

$output .= '</div>';

return $output;

} 

add_shortcode('vsel_past_events', 'vsel_past_events_shortcode');

?>