<?php
// If uninstall is not called from WordPress, exit 
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) { 
exit(); 
} 

// Delete custom post meta 
delete_post_meta_by_key( 'event-date' );
delete_post_meta_by_key( 'event-date-hide' );
delete_post_meta_by_key( 'event-time' );
delete_post_meta_by_key( 'event-location' );
delete_post_meta_by_key( 'event-link' );
delete_post_meta_by_key( 'event-link-label' );

// Delete events
global $wpdb;
$wpdb->query( "DELETE FROM {$wpdb->posts} WHERE post_type = 'event'" ); 
?>
