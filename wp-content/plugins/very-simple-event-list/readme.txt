=== Very Simple Event List ===
Contributors: Guido07111975
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=donation%40guidovanderleest%2enl
Version: 3.2
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.7
Tested up to: 4.4
Stable tag: trunk
Tags: simple, upcoming, past, event, list, manager, datepicker


== Changelog ==
= Version 3.2 =
* request and fix: changed alignment of next and prev link

= Version 3.1 =
* localized the datepicker
* localized dateformat: European and US dateformat
* request: added checkbox to hide date in frontend
* added a PayPal donate link
* updated readme file

= Version 3.0 =
* reloated file vsel_style to folder css
* updated file vsel
* updated readme file

= Version 2.9 =
* file style: bugfix, in some cases event was not displayed full width (thanks Emmy)
* file style: added class for event title

= Version 2.8 =
* request: added shortcode attribute to set number of event on page
* request: added class for each meta-tag paragraph
* updated file vsel_style
* updated file readme

= Version 2.7 =
* removed translations: plugin now supports WordPress language packs
* added event categories: display events from certain categories using a shortcode attribute, more info about this at the Installation section 
* added custom URL link label to replace the static 'More info'
* updated file readme

= Version 2.6 =
* added file uninstall.php so settings in database (including all events) are removed when uninstalling plugin
* datepicker update

= Version 2.5 =
* updated Data Validation and Escaping
* fixed mistake in French translation (thanks Sylva) 

= Version 2.4 =
* fix: in previous versions content was displayed without linebreaks (thanks Jesper) 

= Version 2.3 =
* changed text domain for the wordpress.org translation system
* fixed next/prev posts navigation

= Version 2.2 =
* updated files vsel_shortcode and vsel_past_events_shortcode
* added Czech translation (thanks Jiri Bires)
* updated language files

= Version 2.1 =
* files vsel_shortcode and vsel_past_events_shortcode: replaced echo with return (thanks Sebastian Schulte)

= Version 2.0 =
* for frontend you can set date format in WP dashboard via Settings > General
* updated files vsel_shortcode and vsel_past_events_shortcode 
* updated FAQ
 
= Version 1.9 =
* updated file readme

= Version 1.8 =
* updated language files
* added French translation (thanks Claire Delavallee)
* added Portuguese translation (thanks Marta Ferreira)

= Version 1.7 =
* added Brazilian Portuguese translation (thanks Fernando Sousa)
* added Ukrainian translation (thanks Kuda Poltava Team)

= Version 1.6 =
* added German translation (thanks Andrea)
* updated FAQ
 
= Version 1.5 =
* file vsel.php: changed date format in backend
* updated FAQ

= Version 1.4 =
* request: display past events too
* added file vsel_past_events_shortcode.php: now you can display past events too
* file vsel.php: increased max input from 50 to 150 characters
* file vsel.php: added sanitize_text_field and esc_url to input
* added Swedish translation (thanks Cecilia Svensson)

= Version 1.3 =
* file vsel.php: changed URL validation from sanitize_text_field into esc_url
* file vsel_shortcode.php: changed display of URL in frontend
* updated language files

= Version 1.2 =
* request: add field for event URL (link)
* updated FAQ
* updated language files

= Version 1.1 =
* added featured image
* added pagination
* several small adjustments
* updated FAQ

= Version 1.0 =
* first stable release


== DESCRIPTION ==
This is a very simple plugin to add a list of your upcoming or past events in your WordPress blog. 

Besides the default title and description you can set event date, event time, event location, event URL, event URL label, event category and featured image.

Use shortcode `[vsel]` to display your upcoming events on a page.

Use shortcode `[vsel_past_events]` to display your past events on a page.

Shortcode accepts attributes too. You can find more info about this at the Installation section.

= Question? = 
Please take a look at the FAQ section.

= Translation =
Not included but plugin supports WordPress language packs.

= Credits =
Without the WordPress codex and help from the WordPress community I was not able to develop this plugin, so: thank you!

Enjoy!


== INSTALLATION == 
After installation go to Events and start adding your events.

On right side (Event Info) you can set event date, event time, event location, event URL, event URL label and featured image.

It also has a checkbox to hide event date in frontend. 

Besides this you can set event categories there.

Use shortcode `[vsel]` to display your upcoming events on a page.

Use shortcode `[vsel_past_events]` to display your past events on a page.

= Personalize the shortcode using attributes =

To display events from certain event categories: `[vsel event_cat="first-category, second-category"]`

You should enter the category slug (a slug is not always the same as category name).

To set amount of events per page: `[vsel posts_per_page=5]` 

This will overwrite amount set in Settings > Reading.


== Frequently Asked Questions ==
= How can I change date format? =
You can set date format in WP dashboard via Settings > General.

The datepicker and date input field in backend accept 2 numeric date formats: European (day-month-year) and US (year-month-day).

If the date format set in WP dashboard is not numeric it will be changed into 1 of these 2 numeric date formats.  

= How do I set plugin language? =
Very Simple Event List uses the WP Dashboard language, set in Settings > General. If plugin language pack is not available, event will display in English.

= Can I change event list layout? =
You can change the layout (CSS) of your event list using for example the [Very Simple Custom Style](https://wordpress.org/plugins/very-simple-custom-style) plugin.

= How can I set number of events on a page? =
You can find more info about this at the Installation section.

= Can I display certain events only? =
You can find more info about this at the Installation section.

= Why is a single event not displayed properly? =
VSEL is build to display events as list on a page using a shortcode. 

Plugin style is not supported and additional fields are not displayed when viewing a single event via dashboard.

= Are all fields required? =
No, most fields are not required fields.

= What's an event URL and how do I enter it? =
It's just a link to another website for more info. You can enter the complete URL or without the http part (the http part will be auto added if not entered).

Don't forget field for event URL label. If empty label will be 'More info'.

= Can I list both upcoming and past events? =
You can list upcoming or past events in frontend using different shortcodes. 

You should not use both shortcodes on the same page.

= How do I list upcoming and past events in a template file? =
For upcoming events: `<?php echo do_shortcode( '[vsel]' ); ?>` 

For past events: `<?php echo do_shortcode( '[vsel_past_events]' ); ?>`

= What happens with my events when I uninstall VSEL plugin? =
When you uninstall plugin via WP dashboard all data is removed from database, so including all events.

It removes all posts with (custom) post type 'event'.

So don't uninstall plugin if you want to keep your events.

= How can I make a donation? =
You like my plugin and you're willing to make a donation? Nice! There's a PayPal donate link on the WordPress plugin page and my website.

= Other question or comment? =
Please open a topic in plugin forum.


== Screenshots == 
1. Very Simple Event List in frontend (using Twenty Sixteen theme).
2. Very Simple Event List in dashboard.
3. Very Simple Event List in dashboard.